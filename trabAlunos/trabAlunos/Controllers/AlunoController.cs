﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using trabAlunos.Models;

namespace trabAlunos.Controllers
{
    public class AlunoController : Controller
    {
        
        public IActionResult Index()
        {
            return View();
        }

        public JsonResult CarregarAlunos()
        {
            Models.Aluno a = new Models.Aluno();

            List<Aluno> alunos = a.ObterTodos();

            return Json(alunos);
        }

        public JsonResult FindByName([FromBody]string nome)
        {
            Models.Aluno a = new Models.Aluno();
            List<Aluno> alunos = a.ObterTodos();

            List<Aluno> resultFind = a.FindByName(alunos, nome.ToUpper());

            return Json(resultFind);
        }






    }
}
