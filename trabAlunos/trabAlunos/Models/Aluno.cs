﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace trabAlunos.Models
{


        public class Aluno
        {
            Int64 _id;
            string _nome;

            public long Id { get => _id; set => _id = value; }
            public string Nome { get => _nome; set => _nome = value; }

            public List<Aluno> FindByName(List<Aluno> alunos, string nome)
            {
                List<Aluno> resp = new List<Aluno>();
                /*for(int i=0; i < alunos.Count; i++)
                {
                if (alunos[i].Nome.Contains(nome))
                    resp.Add(alunos[i]);
                }*/

            foreach(Aluno a in alunos)
            {
                if(a.Nome.Contains(nome))
                    resp.Add(a);
            }


                return resp;
            }
            public List<Aluno> ObterTodos()
            {
                List<Aluno> alunos = new List<Aluno>();

                alunos.Add(new Aluno() { Id = 260927082, Nome = "ANDREY VITOR DE LIMA ARAÚJO" });
                alunos.Add(new Aluno() { Id = 260928330, Nome = "GABRIEL GONÇALVES OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261028740, Nome = "SILMARA ANDRESSA BULHÕES DA SILVA FURINI" });
                alunos.Add(new Aluno() { Id = 261030485, Nome = "RENÊ FERREIRA DE ALMEIDA" });
                alunos.Add(new Aluno() { Id = 261030639, Nome = "CARLOS EDUARDO SCHEIDEMANTEL MALAQUIAS" });
                alunos.Add(new Aluno() { Id = 261030728, Nome = "MATHEUS FERNANDES FERRAZ" });
                alunos.Add(new Aluno() { Id = 261131290, Nome = "DANIEL RODRIGUES DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261132431, Nome = "EVANDRO RICARDO SAPIA JÚNIOR" });
                alunos.Add(new Aluno() { Id = 261232657, Nome = "ANTONIO HENRIQUE SAMPAIO" });
                alunos.Add(new Aluno() { Id = 261232673, Nome = "VINÍCIUS HENRIQUE MANFREDINI" });
                alunos.Add(new Aluno() { Id = 261233270, Nome = "FELLIPE FALCONE SOUZA" });
                alunos.Add(new Aluno() { Id = 261233742, Nome = "KELLY ROMUALDA VIEIRA" });
                alunos.Add(new Aluno() { Id = 261234021, Nome = "JOÃO ANDRÉ MARTINS DIAS E SILVA" });
                alunos.Add(new Aluno() { Id = 261234200, Nome = "SARITA PEREIRA GUERRA" });
                alunos.Add(new Aluno() { Id = 261334816, Nome = "WILIAN CAPELLI MUNIZ" });
                alunos.Add(new Aluno() { Id = 261334832, Nome = "MATHEUS AUGUSTO DE GÓIS SILVA" });
                alunos.Add(new Aluno() { Id = 261335600, Nome = "GUILHERME PRADO ROMA" });
                alunos.Add(new Aluno() { Id = 261335626, Nome = "VINÍCIUS BREDA SILVA" });
                alunos.Add(new Aluno() { Id = 261335901, Nome = "WELLINGTON DE BARROS RAMOS FILHO" });
                alunos.Add(new Aluno() { Id = 261336037, Nome = "RAUL FRANCISCO DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261436414, Nome = "ALEKSANDER SILVA GOMES" });
                alunos.Add(new Aluno() { Id = 261436511, Nome = "ANTONIO DA SILVA JÚNIOR" });
                alunos.Add(new Aluno() { Id = 261436643, Nome = "CELSO JÚNIOR SOARES SANTANA" });
                alunos.Add(new Aluno() { Id = 261436678, Nome = "MURILO RODRIGUES WELLER" });
                alunos.Add(new Aluno() { Id = 261436708, Nome = "ISMAEL CORREIA DA SILVA" });
                alunos.Add(new Aluno() { Id = 261437038, Nome = "RAFAEL HIDEAKI FONSECA NAKAHARA" });
                alunos.Add(new Aluno() { Id = 261437089, Nome = "GERSON VINICIUS CARRION MARQUES" });
                alunos.Add(new Aluno() { Id = 261437399, Nome = "JOSÉ THIAGO PEREIRA JORGE" });
                alunos.Add(new Aluno() { Id = 261437445, Nome = "MÁRIO GEROLDI NETO" });
                alunos.Add(new Aluno() { Id = 261437550, Nome = "PRISCYLA YUMIKO TANOUE TARUMOTO" });
                alunos.Add(new Aluno() { Id = 261437801, Nome = "JOYCE COURAÇA DE SOUZA" });
                alunos.Add(new Aluno() { Id = 261437836, Nome = "LUCAS FELÍCIO ALFINI" });
                alunos.Add(new Aluno() { Id = 261438131, Nome = "MARIO FONSECA CARRARA" });
                alunos.Add(new Aluno() { Id = 261538349, Nome = "PEDRO PONTES MARTINS SOARES" });
                alunos.Add(new Aluno() { Id = 261538365, Nome = "PAMELA MAYUMI SILVA AOYAGUI" });
                alunos.Add(new Aluno() { Id = 261538403, Nome = "CAIO CARDOSO SAMPAIO" });
                alunos.Add(new Aluno() { Id = 261538438, Nome = "ÁLISSON JÚNIOR DA SILVA ANANIAS" });
                alunos.Add(new Aluno() { Id = 261538535, Nome = "GUILHERME BERNARDELLI NEUHAUS" });
                alunos.Add(new Aluno() { Id = 261538594, Nome = "AMANDA MATURANO MOLINA" });
                alunos.Add(new Aluno() { Id = 261538667, Nome = "MAURÍCIO HENRIQUE OLIVEIRA MONTI" });
                alunos.Add(new Aluno() { Id = 261538713, Nome = "FELIPE MASSAKI TATIZAWA" });
                alunos.Add(new Aluno() { Id = 261538756, Nome = "NATAN FELIPE SILVA" });
                alunos.Add(new Aluno() { Id = 261538780, Nome = "WESLEY FERREIRA DO NASCIMENTO" });
                alunos.Add(new Aluno() { Id = 261538837, Nome = "KAIQUE DA SILVA GARCIA" });
                alunos.Add(new Aluno() { Id = 261538853, Nome = "VINÍCIUS BASTAZIN ARAUJO" });
                alunos.Add(new Aluno() { Id = 261538888, Nome = "JONATHAN CRISTIANO PINHEIRO" });
                alunos.Add(new Aluno() { Id = 261538896, Nome = "BRUNO DA SILVA BUENO" });
                alunos.Add(new Aluno() { Id = 261538969, Nome = "LEONARDO PEPINELLI DA SILVA" });
                alunos.Add(new Aluno() { Id = 261538985, Nome = "LUÍS HENRIQUE CARVALHO DOMINGUES" });
                alunos.Add(new Aluno() { Id = 261539094, Nome = "PEDRO FURLAN ZANARDO LOUZADA" });
                alunos.Add(new Aluno() { Id = 261539248, Nome = "RAFAEL NAZARENO CORTÊZ" });
                alunos.Add(new Aluno() { Id = 261539329, Nome = "RAFAEL FRANCISCO DOS SANTOS SOUZA" });
                alunos.Add(new Aluno() { Id = 261539361, Nome = "GUILHERME MANZANO SILVA" });
                alunos.Add(new Aluno() { Id = 261539388, Nome = "VÍCTOR PEREIRA ALMEIDA DE CARVALHO" });
                alunos.Add(new Aluno() { Id = 261539515, Nome = "MATHEUS FRANCISCO FERREIRA DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261539612, Nome = "BRENON DOS SANTOS PEREZ TAVARES" });
                alunos.Add(new Aluno() { Id = 261539833, Nome = "PEDRO RENATO RODRIGUES DE SOUSA" });
                alunos.Add(new Aluno() { Id = 261539841, Nome = "NATAN DOS SANTOS PEREZ TAVARES" });
                alunos.Add(new Aluno() { Id = 261539884, Nome = "ITALO CESAR PIOVAN ROCHA" });
                alunos.Add(new Aluno() { Id = 261539922, Nome = "GABRIEL MACEDO DE ALMEIDA" });
                alunos.Add(new Aluno() { Id = 261539930, Nome = "GIOVANI VAITKEVICIUS" });
                alunos.Add(new Aluno() { Id = 261539973, Nome = "ADRIANO PASOTI PATRONI" });
                alunos.Add(new Aluno() { Id = 261640127, Nome = "FÚLVIO FANELLI" });
                alunos.Add(new Aluno() { Id = 261640135, Nome = "FELIPE OCANHA PEREIRA" });
                alunos.Add(new Aluno() { Id = 261640143, Nome = "LIGIA FERNANDA DE SOUZA CAVALCANTE" });
                alunos.Add(new Aluno() { Id = 261640151, Nome = "PEDRO AUGUSTO SITOLINO" });
                alunos.Add(new Aluno() { Id = 261640232, Nome = "GUILHERME BOLOGNANI DE SOUZA" });
                alunos.Add(new Aluno() { Id = 261640259, Nome = "GABRIEL NUNES BASTOS CAPARROZ" });
                alunos.Add(new Aluno() { Id = 261640330, Nome = "GUSTAVO APARECIDO BARRETO LIMA" });
                alunos.Add(new Aluno() { Id = 261640453, Nome = "RENAN LUCAS SAPIA SANTOS" });
                alunos.Add(new Aluno() { Id = 261640534, Nome = "BRUNO REN YOSHINO" });
                alunos.Add(new Aluno() { Id = 261640615, Nome = "JULIO CESAR BINDANDI ALVES" });
                alunos.Add(new Aluno() { Id = 261640755, Nome = "DANIEL ALEXANDRE RAMIRO" });
                alunos.Add(new Aluno() { Id = 261640780, Nome = "FELIPE SANTOS TADEI" });
                alunos.Add(new Aluno() { Id = 261640810, Nome = "DOUGLAS KENTA IWASAKI" });
                alunos.Add(new Aluno() { Id = 261640828, Nome = "VICTOR MATIAS DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261640836, Nome = "LEONARDO BRITO DA SILVA" });
                alunos.Add(new Aluno() { Id = 261640917, Nome = "VICTOR PRADO DINALLO" });
                alunos.Add(new Aluno() { Id = 261640925, Nome = "HUGO FELIPPE DE SOUZA CRUZ" });
                alunos.Add(new Aluno() { Id = 261640941, Nome = "HELIO MURILO SILVA DE BARROS" });
                alunos.Add(new Aluno() { Id = 261640968, Nome = "PAULO HENRIQUE FERNANDES DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261640976, Nome = "DENIS FERNANDO DE OLIVEIRA ANDRADE" });
                alunos.Add(new Aluno() { Id = 261641050, Nome = "MONICA RODRIGUES DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261641077, Nome = "THIAGO LUZ DE SOUSA" });
                alunos.Add(new Aluno() { Id = 261641085, Nome = "BRENO FERNANDES DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261641115, Nome = "GIVALDO RIBEIRO DE SOUZA" });
                alunos.Add(new Aluno() { Id = 261641131, Nome = "LUCAS DA SILVA OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261641204, Nome = "HENRIQUE SHIRASAKI DA MAIA" });
                alunos.Add(new Aluno() { Id = 261641220, Nome = "GABRIEL BERNARDES TARDIM" });
                alunos.Add(new Aluno() { Id = 261641280, Nome = "HUGO FERNANDO PEREIRA" });
                alunos.Add(new Aluno() { Id = 261641298, Nome = "VICTOR VINICIUS DA SILVA CHAVES" });
                alunos.Add(new Aluno() { Id = 261641301, Nome = "LUCAS JOSÉ DA COSTA" });
                alunos.Add(new Aluno() { Id = 261641310, Nome = "JOÃO ANTONIO HOLANDA SOLLER" });
                alunos.Add(new Aluno() { Id = 261641344, Nome = "FELIPE JUHASC DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261641352, Nome = "JOÃO VICTOR DA SILVA MUCHIUTTI" });
                alunos.Add(new Aluno() { Id = 261641360, Nome = "JAIR GANZAROLLI JÚNIOR" });
                alunos.Add(new Aluno() { Id = 261641522, Nome = "RODRIGO SOPRANI SANCHEZ" });
                alunos.Add(new Aluno() { Id = 261741560, Nome = "JIFFERSON NAVARRO DE ARAUJO" });
                alunos.Add(new Aluno() { Id = 261741578, Nome = "LAURA LOPES STIPSKY" });
                alunos.Add(new Aluno() { Id = 261741608, Nome = "BÁRBARA DE CARVALHO ALEXANDRE" });
                alunos.Add(new Aluno() { Id = 261741667, Nome = "RENAN FERRARI MARTINES" });
                alunos.Add(new Aluno() { Id = 261741705, Nome = "CAIO BASSO RIGAZZO" });
                alunos.Add(new Aluno() { Id = 261741713, Nome = "DANIEL KROM ANTONIO" });
                alunos.Add(new Aluno() { Id = 261741748, Nome = "GUSTAVO DILLIO LEITÃO" });
                alunos.Add(new Aluno() { Id = 261741764, Nome = "LUCAS ALMEIDA PORTILHO BARCELOS" });
                alunos.Add(new Aluno() { Id = 261741853, Nome = "HUDSON TROMBETA RIBEIRO" });
                alunos.Add(new Aluno() { Id = 261741888, Nome = "GABRIEL MIGUEL NAVAS" });
                alunos.Add(new Aluno() { Id = 261741934, Nome = "LEANDRO CANTIERO CAMPAGNOLO" });
                alunos.Add(new Aluno() { Id = 261741950, Nome = "LUCAS BATALINI FERREIRA DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261741985, Nome = "LUCAS EDUARDO DE MORAIS FERRER" });
                alunos.Add(new Aluno() { Id = 261742000, Nome = "BRUNO MARQUES DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261742035, Nome = "PEDRO AUGUSTO DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261742078, Nome = "ANDRESSA HISAE TSUKASAKI" });
                alunos.Add(new Aluno() { Id = 261742183, Nome = "MATHEUS CESAR DONHA" });
                alunos.Add(new Aluno() { Id = 261742205, Nome = "EDUARDO BRUNO DO NASCIMENTO CRUZ" });
                alunos.Add(new Aluno() { Id = 261742221, Nome = "RODOLPHO VINCENZO VERÍSSIMO CARAVANTE" });
                alunos.Add(new Aluno() { Id = 261742310, Nome = "VIVIAN MATIAS DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261742388, Nome = "ANA CAROLINA DE SOUZA SILVA" });
                alunos.Add(new Aluno() { Id = 261742396, Nome = "FERNANDO PAULINO ALDUINO JUNIOR" });
                alunos.Add(new Aluno() { Id = 261742418, Nome = "WESLEY MIRANDA MOURA" });
                alunos.Add(new Aluno() { Id = 261742426, Nome = "DIEGO ANTONIO ARAUJO HALBERKONI" });
                alunos.Add(new Aluno() { Id = 261742442, Nome = "KEVIN DE LIMA ALCANTARA" });
                alunos.Add(new Aluno() { Id = 261742469, Nome = "RODRIGO COLADELLO PEREIRA" });
                alunos.Add(new Aluno() { Id = 261742477, Nome = "ITALO ALESSANDRO BARBOZA FERREIRA" });
                alunos.Add(new Aluno() { Id = 261742485, Nome = "GUSTAVO MATHEUS DOS SANTOS PERENHA" });
                alunos.Add(new Aluno() { Id = 261742493, Nome = "VITOR KUBINYETE BARBOSA" });
                alunos.Add(new Aluno() { Id = 261742507, Nome = "LEONARDO MARANZATE SALAZAR" });
                alunos.Add(new Aluno() { Id = 261742515, Nome = "VITOR MORENO NEGRI" });
                alunos.Add(new Aluno() { Id = 261742566, Nome = "MURILO AUGUSTO PADOVAN AMARAL" });
                alunos.Add(new Aluno() { Id = 261742612, Nome = "GUILHERME FELIPE DE ASSIS ANDRADE" });
                alunos.Add(new Aluno() { Id = 261742647, Nome = "MAURICIO BARBOSA DOS SANTOS JUNIOR" });
                alunos.Add(new Aluno() { Id = 261742655, Nome = "HIGOR DANIEL PEREIRA BRITO" });
                alunos.Add(new Aluno() { Id = 261742698, Nome = "GUSTAVO FELIPE MAIOLI" });
                alunos.Add(new Aluno() { Id = 261742710, Nome = "LEONARDO CANHIZARES DIAS SERRANO" });
                alunos.Add(new Aluno() { Id = 261742736, Nome = "GUILHERME SOARES DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261742744, Nome = "RENAN TAVARES DA SILVA" });
                alunos.Add(new Aluno() { Id = 261742752, Nome = "FABRICIO DE CARVALHO SOUSA" });
                alunos.Add(new Aluno() { Id = 261742825, Nome = "LEONARDO FERREIRA DE ALMEIDA" });
                alunos.Add(new Aluno() { Id = 261742833, Nome = "LUCAS CARVALHO MEDEIROS" });
                alunos.Add(new Aluno() { Id = 261742868, Nome = "ANGÉLICA DA SILVA OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261742876, Nome = "DAVÍ APARECIDO DA SILVA" });
                alunos.Add(new Aluno() { Id = 261742884, Nome = "DIEGO LEBEDENCO" });
                alunos.Add(new Aluno() { Id = 261742892, Nome = "JOÃO BATISTA DE CARVALHO NETTO" });
                alunos.Add(new Aluno() { Id = 261742914, Nome = "LEONARDO DE MATOS FIDELIS" });
                alunos.Add(new Aluno() { Id = 261742922, Nome = "LUANA PORTO CORREIA" });
                alunos.Add(new Aluno() { Id = 261742930, Nome = "ANDERSON JUNIOR LIMA COELHO DE FARIAS" });
                alunos.Add(new Aluno() { Id = 261742949, Nome = "WILLIAN CHRISTIAN SOUZA MASSOCHINI" });
                alunos.Add(new Aluno() { Id = 261742957, Nome = "JOÃO PAULO DE MESQUITA" });
                alunos.Add(new Aluno() { Id = 261742965, Nome = "ANDRE TANAKA CAMPOS" });
                alunos.Add(new Aluno() { Id = 261742981, Nome = "RENAN JUNIOR DOS SANTOS BATISTA" });
                alunos.Add(new Aluno() { Id = 261742990, Nome = "KAÍQUE FERNANDO SILVA PEREIRA" });
                alunos.Add(new Aluno() { Id = 261743007, Nome = "JOÃO VITOR VIANA DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261743074, Nome = "VINICIUS PANES NAKANDAKARE" });
                alunos.Add(new Aluno() { Id = 261743090, Nome = "NYECHYNNA NATYELLY DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261810022, Nome = "RAFAEL BIGESCHI DE ALMEIDA" });
                alunos.Add(new Aluno() { Id = 261810049, Nome = "JOÃO VICTOR PULHEIS SIERRA" });
                alunos.Add(new Aluno() { Id = 261810057, Nome = "LEONARDO BIGESCHI DE ALMEIDA" });
                alunos.Add(new Aluno() { Id = 261810090, Nome = "RODRIGO DE LIMA PEREIRA" });
                alunos.Add(new Aluno() { Id = 261810103, Nome = "VINÍCIUS PADOVAN TERTULIANO DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261810120, Nome = "LUCAS FELIPE SILVA GOMES" });
                alunos.Add(new Aluno() { Id = 261810154, Nome = "NATHAN DE OLIVEIRA MOREIRA" });
                alunos.Add(new Aluno() { Id = 261810235, Nome = "LUCAS FALCÃO DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261810286, Nome = "DANILO SANTILHO DA SILVA" });
                alunos.Add(new Aluno() { Id = 261810391, Nome = "RONALDO DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261810405, Nome = "CARLOS EDUARDO LIMA NASCIMENTO" });
                alunos.Add(new Aluno() { Id = 261810413, Nome = "GABRIEL PELLIN CAETANO" });
                alunos.Add(new Aluno() { Id = 261810421, Nome = "PEDRO LEITE DO NASCIMENTO" });
                alunos.Add(new Aluno() { Id = 261810456, Nome = "SANDRA DE OLIVEIRA SOUZA" });
                alunos.Add(new Aluno() { Id = 261810472, Nome = "JOÃO PEDRO GONÇALVES DE MELO" });
                alunos.Add(new Aluno() { Id = 261810480, Nome = "MATHEUS MILAN BATISTA" });
                alunos.Add(new Aluno() { Id = 261810529, Nome = "VITOR HUGO BORTOLETO RAMOS" });
                alunos.Add(new Aluno() { Id = 261810650, Nome = "GUILHERME CAMARGO GONZALEZ" });
                alunos.Add(new Aluno() { Id = 261810669, Nome = "CAIO RESENDE" });
                alunos.Add(new Aluno() { Id = 261810723, Nome = "GUILHERME FERREIRA BIONDE" });
                alunos.Add(new Aluno() { Id = 261810740, Nome = "NATHALIA RODRIGUES DE ALMEIDA" });
                alunos.Add(new Aluno() { Id = 261810766, Nome = "HENRIQUE AUGUSTO BONIFÁCIO TERÊNCIO" });
                alunos.Add(new Aluno() { Id = 261810790, Nome = "LEONARDO CUSTODIO DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261810804, Nome = "PEDRO HENRIQUE DIAS MACENA" });
                alunos.Add(new Aluno() { Id = 261810812, Nome = "PEDRO AUGUSTO DA SILVA ROCHA" });
                alunos.Add(new Aluno() { Id = 261810820, Nome = "BRUNO ARANHA SOUZA" });
                alunos.Add(new Aluno() { Id = 261810847, Nome = "BRUNO HENRIQUE DOS SANTOS SILVA" });
                alunos.Add(new Aluno() { Id = 261810910, Nome = "RIAN APARECIDO CORREIA SILVA" });
                alunos.Add(new Aluno() { Id = 261810960, Nome = "DIEGO DE SOUZA SANTOS" });
                alunos.Add(new Aluno() { Id = 261810987, Nome = "FLAVIO ROSO GONÇALVES" });
                alunos.Add(new Aluno() { Id = 261811010, Nome = "CÉSAR AUGUSTO APARECIDO PEREIRA" });
                alunos.Add(new Aluno() { Id = 261811045, Nome = "VINICIUS HENRIQUE COUTINHO ALVES" });
                alunos.Add(new Aluno() { Id = 261811053, Nome = "FELIPE DOS SANTOS GEROLDI" });
                alunos.Add(new Aluno() { Id = 261811207, Nome = "DOUGLAS TOSHIO BOMTEMPO TATEISI" });
                alunos.Add(new Aluno() { Id = 261811215, Nome = "PAULO HENRIQUE VIEIRA BARRETO" });
                alunos.Add(new Aluno() { Id = 261811282, Nome = "VITOR HUGO LOPES DA SILVA" });
                alunos.Add(new Aluno() { Id = 261811304, Nome = "MILEID DE JESUS" });
                alunos.Add(new Aluno() { Id = 261811339, Nome = "ALEX MAFRA DE JESUS" });
                alunos.Add(new Aluno() { Id = 261811410, Nome = "GRAZIANO FERREIRA BEZERRA" });
                alunos.Add(new Aluno() { Id = 261811428, Nome = "JORGE MATEUS DE SOUZA SANTOS" });
                alunos.Add(new Aluno() { Id = 261811436, Nome = "PEDRO ALMEIDA MOLINA HERRERA REIS" });
                alunos.Add(new Aluno() { Id = 261821075, Nome = "VILIAM HUGO CABRERA HIRATA" });
                alunos.Add(new Aluno() { Id = 261821091, Nome = "PEDRO HENRIQUE OLIVEIRA BELATO" });
                alunos.Add(new Aluno() { Id = 261821113, Nome = "MATHEUS FERREIRA MIOTO" });
                alunos.Add(new Aluno() { Id = 261821121, Nome = "BRUNO EIJI SASAKI DIAS" });
                alunos.Add(new Aluno() { Id = 261821130, Nome = "JAMIL MURAD DA SILVA NETO" });
                alunos.Add(new Aluno() { Id = 261821148, Nome = "YURI JÚNIOR SAITO" });
                alunos.Add(new Aluno() { Id = 261821199, Nome = "CRISTIAN DOROTEU DO CARMO SILVA" });
                alunos.Add(new Aluno() { Id = 261821237, Nome = "HUGO OLIVEIRA DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261821318, Nome = "BRUNO DO CARMO CAVALHEIRO" });
                alunos.Add(new Aluno() { Id = 261821369, Nome = "BRENNER BARROS BACIGA" });
                alunos.Add(new Aluno() { Id = 261911465, Nome = "GABRIEL PUGAS DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261911503, Nome = "ENZO VANNUCCI BENVENGO" });
                alunos.Add(new Aluno() { Id = 261911570, Nome = "CARLOS EDUARDO CORREIA DE SOUZA" });
                alunos.Add(new Aluno() { Id = 261911600, Nome = "LEONARDO BRIAN DO NASCIMENTO CRUZ" });
                alunos.Add(new Aluno() { Id = 261911660, Nome = "HENRIQUE MARTINS RODRIGUES" });
                alunos.Add(new Aluno() { Id = 261911716, Nome = "LUAN ALVES BONIFÁCIO" });
                alunos.Add(new Aluno() { Id = 261911724, Nome = "VÍTOR ASSIS CAMARGO" });
                alunos.Add(new Aluno() { Id = 261911732, Nome = "WALYSON KAUE GARBO" });
                alunos.Add(new Aluno() { Id = 261911759, Nome = "VICTOR TAVEIRA RODRIGUES" });
                alunos.Add(new Aluno() { Id = 261911767, Nome = "VITOR DE ARAUJO ROMÃO GUILHERMO" });
                alunos.Add(new Aluno() { Id = 261911775, Nome = "JOÃO VITOR DA SILVA" });
                alunos.Add(new Aluno() { Id = 261911791, Nome = "NATHAN ALISSON GONÇALVES" });
                alunos.Add(new Aluno() { Id = 261911813, Nome = "FELIPE YUJI VICENTE UMEMURA" });
                alunos.Add(new Aluno() { Id = 261911856, Nome = "ALEXANDRE AUGUSTO FEITOSA LOPES" });
                alunos.Add(new Aluno() { Id = 261911880, Nome = "ANTONIO INÁCIO RODRIGUES NETO" });
                alunos.Add(new Aluno() { Id = 261911899, Nome = "DANIEL ROSAN TROMBINI" });
                alunos.Add(new Aluno() { Id = 261911953, Nome = "MIRIAN ROSA DE OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261911961, Nome = "BRUNO ALVES SILVA" });
                alunos.Add(new Aluno() { Id = 261911970, Nome = "LUCAS GONÇALVES LEÃO" });
                alunos.Add(new Aluno() { Id = 261911988, Nome = "PAULO HENRIQUE BERGAMINI DE ALMEIDA" });
                alunos.Add(new Aluno() { Id = 261912003, Nome = "PEDRO ARTHUR UEMA DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261912011, Nome = "GUSTAVO APARECIDO DA SILVA MELO" });
                alunos.Add(new Aluno() { Id = 261912020, Nome = "EMANUEL HENRIQUE MAGRI DA SILVA" });
                alunos.Add(new Aluno() { Id = 261912038, Nome = "IGOR XAVIER DA SILVA" });
                alunos.Add(new Aluno() { Id = 261912046, Nome = "PEDRO HENRIQUE DE BARROS RUANO" });
                alunos.Add(new Aluno() { Id = 261912054, Nome = "EDUARDO DE LIMA TAKAYA" });
                alunos.Add(new Aluno() { Id = 261912062, Nome = "JOÃO PEDRO FAVARÃO VIEIRA" });
                alunos.Add(new Aluno() { Id = 261912097, Nome = "VITOR FULOP COSTA" });
                alunos.Add(new Aluno() { Id = 261912100, Nome = "HERNANDES MONTAÑO SILVA" });
                alunos.Add(new Aluno() { Id = 261912119, Nome = "FELIPE DE OLIVEIRA SILVA" });
                alunos.Add(new Aluno() { Id = 261912135, Nome = "VITOR HUGO TSUYOSHI GUIMARÃES HAGA" });
                alunos.Add(new Aluno() { Id = 261912151, Nome = "EDUARDA DE SOUZA" });
                alunos.Add(new Aluno() { Id = 261912178, Nome = "ANDRÉ ARIAS DELFINO DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261912194, Nome = "DIONE SANTOS DE MORAES" });
                alunos.Add(new Aluno() { Id = 261912208, Nome = "GUSTAVO DE LIMA DESTRO" });
                alunos.Add(new Aluno() { Id = 261912216, Nome = "MARIA BEATRIZ SOUZA FRANÇA" });
                alunos.Add(new Aluno() { Id = 261912240, Nome = "CARLOS HENRIQUE DE CASTRO PEREIRA" });
                alunos.Add(new Aluno() { Id = 261912283, Nome = "AILTON FABRICIO DA SILVA JÚNIOR" });
                alunos.Add(new Aluno() { Id = 261912291, Nome = "GUSTAVO GOMES DO AMARAL" });
                alunos.Add(new Aluno() { Id = 261912348, Nome = "EMANUEL SANT'ANNA BATISTA" });
                alunos.Add(new Aluno() { Id = 261912356, Nome = "MATHEUS VALERA PEREIRA" });
                alunos.Add(new Aluno() { Id = 261912364, Nome = "PEDRO RICARDO CASTELÃO DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261912372, Nome = "MARCOS ANTONIO BRATIFISCH JÚNIOR" });
                alunos.Add(new Aluno() { Id = 261912380, Nome = "JOSÉ ANTONIO JÚNIOR BARROS SOUZA" });
                alunos.Add(new Aluno() { Id = 261912496, Nome = "LUCAS PAZ RODELLA" });
                alunos.Add(new Aluno() { Id = 261912534, Nome = "THIAIA RENATA ZANONI" });
                alunos.Add(new Aluno() { Id = 261912577, Nome = "IRINEU DE ALMEIDA JÚNIOR" });
                alunos.Add(new Aluno() { Id = 261922408, Nome = "WASHINGTON LUIZ DELTREJO FARIA" });
                alunos.Add(new Aluno() { Id = 261922432, Nome = "VINÍCIUS EIDI TUBONO" });
                alunos.Add(new Aluno() { Id = 261922440, Nome = "ALCIDES AUGUSTO NOGUEIRA DOS SANTOS" });
                alunos.Add(new Aluno() { Id = 261922459, Nome = "WELITON DE OLIVEIRA PERRETTI" });
                alunos.Add(new Aluno() { Id = 261922467, Nome = "VINICIUS NOGUEIRA VELASQUES" });
                alunos.Add(new Aluno() { Id = 261922505, Nome = "JOÃO MARCOS VALIN DA ROCHA" });
                alunos.Add(new Aluno() { Id = 261922513, Nome = "RAFAEL PAULO DOS SANTOS OLIVEIRA" });
                alunos.Add(new Aluno() { Id = 261922521, Nome = "RAPHAEL GUIMARÃES VESCHI" });
                alunos.Add(new Aluno() { Id = 261922548, Nome = "GABRIEL DOS SANTOS MACHADO" });
                alunos.Add(new Aluno() { Id = 261922556, Nome = "VITOR NAKAMURA MORETTO" });
                alunos.Add(new Aluno() { Id = 261922580, Nome = "LARRY DE ALMEIDA SARAIVA JUNIOR" });
                alunos.Add(new Aluno() { Id = 261922599, Nome = "ARITANA AGUIAR LEÃO" });
                alunos.Add(new Aluno() { Id = 261922610, Nome = "IAN MARRAFON LINÁRIO LEAL" });
                alunos.Add(new Aluno() { Id = 261922629, Nome = "FELIPE FIDELIS NUNES SOUZA" });

                return alunos;
            }
        }
}
