﻿

var index = {
    init: function () {
        //index.carregarAluno();
        document.getElementById("btnGetAll").onclick = index.carregarAluno;
        document.getElementById("btnClear").onclick = index.limparFormulario;
        document.getElementById("btnFind").onclick = index.find;
        document.getElementById("inpFind2").onkeyup = index.find2;
        //document.getElementById("inpFind2").addEventListener("keyup", teste(document.getElementById("inpFind2").value));

        //document.getElementById("checkMestre").change = index.selecao;

        document.getElementById("checkMestre").addEventListener("change", index.selecao);
       
    },

    carregarAluno: function () {
        var config = {
            method: "GET",
            headers: {
                "Content-type":"application/json; charset=utf-8"
            }

        }
        fetch("Aluno/CarregarAlunos", config)
            .then(function (res) {
                return res.json();

            })
            .then(function (res) {
                index.listarAlunos(res);

            })
            .catch(function () {
                alert("Sua requisição não pode ser atendida.");

            });
    },

    selecao: function () {
        var checkMestre = document.getElementById("checkMestre");
        var listCheckBox = document.getElementsByClassName("checkAluno");

        for (i = 0; i < listCheckBox.length; i++) {
            listCheckBox[i].checked = checkMestre.checked;

        }
        
        /*if ($(this).prop("checked") == true)
            alert("Marcar");

        else
            alert("Desmarcar");*/
    },

    limparFormulario: function () {
        document.getElementById("tbody_tabela").innerHTML = "";
    },


    listarAlunos: function (alunos) {

        var tr = "";

        for (var i = 0; i < alunos.length; i++) {

            tr +=
                "<tr>" +
                "<td> <input class=\"checkAluno\" type=\"checkbox\"/></td>" +
                "<td>" + alunos[i].id + "</td>" +
                "<td>" + alunos[i].nome + "</td>" +
                "</tr>";
        }

        document.getElementById("tbody_tabela").innerHTML = tr;
    },

    find2: function () {

        var nome = document.getElementById("inpFind2").value




        var config = {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(nome)
        }

        fetch("Aluno/FindByName", config)
            .then(function (res) {
                return res.json();

            })
            .then(function (res) {
                index.listarAlunos(res);

            })
            .catch(function () {
                alert("Sua requisição não pode ser atendida.");

            });
    },

    find: function () {

        var inpFind = document.getElementById("inpFind");
        var nome = inpFind.value;
        inpFind.value = "";


        

        var config = {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify(nome)
        }

        fetch("Aluno/FindByName", config)
            .then(function (res) {
                return res.json();

            })
            .then(function (res) {
                index.listarAlunos(res);

            })
            .catch(function () {
                alert("Sua requisição não pode ser atendida.");

            });
    },
}

index.init()